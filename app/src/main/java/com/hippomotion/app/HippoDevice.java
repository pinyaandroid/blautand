package com.hippomotion.app;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jyrki on 2016-02-09.
 *
 */
public class HippoDevice {
    private final static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
                                                      //00001101-0000-1000-8000-00805f9b34fb

    private final static int TIMEOUT_IN_MILLI = 10000;

    Context ctx;

    private BluetoothDevice device;
    private BluetoothSocket socket;

    InputStream is; // = d.getSocket().getInputStream();
    OutputStream os; // = d.getSocket().getOutputStream();

    private String deviceDateTime;
    private String deviceQualifier="";
    private String deviceTemperature;
    private String deviceVoltage;

    static Map<String,String> pairedDevices = new HashMap<>();


    public HippoDevice( BluetoothDevice dev, Context context ){
        ctx = context;

        //todo: We really should throw an exception here.
        if( dev == null ) return;

        device = dev;
        loadStoredDevices();
    }

    public BluetoothSocket getSocket(){
        return socket;
    }

    public String getName(){
        return device.getName();
    }

    public String getDeviceDateTime(){
        return deviceDateTime;
    }

    public void setDeviceQualifier(String qualifier){
        deviceQualifier = qualifier;
    }
    public String getDeviceQualifier(){
        return deviceQualifier;
    }

    public String getDeviceTemperature(){
        return deviceTemperature;
    }

    public String getDeviceVoltage(){
        return deviceVoltage;
    }

    public InputStream getInputStream(){ return is; }

    public OutputStream getOutputStream(){ return os; }

    public String createSecureSocket(){

        try{
            socket = device.createRfcommSocketToServiceRecord(uuid);
            if( !socket.isConnected() ) socket.connect();

            if( socket.isConnected() ){
                is = socket.getInputStream();
                os = socket.getOutputStream();

                flushBuffer();
                updateInfo();
            }
            else {
                Log.d( "Hippo", "Warning *** Socket not yet connected... ***" );
            }
        }
        catch ( IOException e ){
            return e.getMessage();
        }

        return socket.isConnected() ? "Connected" : "Not connected";
    }

    public boolean isConnected(){
        return socket != null && socket.isConnected();
    }

    public boolean close(){
        if( socket != null && socket.isConnected() ){
            try {
                socket.close();
            }
            catch ( IOException e){
                return false;
            }
        }
        return true;
    }

    public void flushBuffer(){
        byte[] buffer = new byte[2048];

        try {
            if ( is != null && is.available() > 0 ) {
                is.read(buffer);
                //mListener.uiLog("Device %s had some extraneous data", d.getDesignation());
                for (int i = 0; i < buffer.length; i++) {
                    buffer[i] = '\0';
                }
            }
        }
        catch (IOException e){
            //todo: log error message...
        }
    }

    public void updateInfo( ){
        if( isConnected() ){

            new Thread(new Runnable() {
                @Override
                public void run() {

                    byte[] buffer = new byte[2048];
                    int bytes = 0;
                    String response="";
                    Pattern regex = Pattern.compile(".*(ERR=\\d$)", Pattern.DOTALL|Pattern.MULTILINE);

                    try {

                        os.write(("GETSYSINFO\n").getBytes());
                        os.flush(); // wait for it...

                        long timeout = System.currentTimeMillis()+TIMEOUT_IN_MILLI;

                        do{
                            if( is.available() > 0 ){
                                bytes = is.read(buffer);
                                response += new String(buffer,0,bytes,"UTF-8");
                            }

                            if( System.currentTimeMillis() > timeout ){
                                break;
                            }

                        } while ( !regex.matcher(response).find() );

                        //if( System.currentTimeMillis() >timeout ){
                        //    Log.d( "Hippo", "Device "+getDesignation()+" Timed out." );
                        //}

                        response = response.replaceAll("(\r\n|\n)", System.getProperty("line.separator") );
                        response = response.replaceAll("(^\n$)", System.getProperty("line.separator") );

                        //Log.d( "Hippo", "Received "+response.length()+" bytes from device "+getDeviceQualifier()+"..." );
                        //Log.d( "Hippo", "Received "+response );


                        if( response.length() > 300 ){ // Semi arbitrary number to ensure enough data is read from stream.
                            Pattern regexDateTime = Pattern.compile("^.*Date/Time  : (.*)$", Pattern.MULTILINE);
                            Pattern regexQualifier = Pattern.compile("^Qualifier  : (.*)$", Pattern.MULTILINE);
                            Pattern regexDeviceTemperature = Pattern.compile("^Sensor Temp: (.*),.*$", Pattern.MULTILINE);
                            Pattern regexRemainingMilliVolts = Pattern.compile("^.*Battery= (\\d*)mV$", Pattern.MULTILINE);

                            Matcher m = regexDateTime.matcher(response);
                            if( m.find() ){
                                deviceDateTime = m.group(1);
                            }

                            m = regexQualifier.matcher(response);
                            if( m.find() ){
                                String qualifier = m.group(1);
                                if( deviceQualifier!= null && !qualifier.contentEquals( deviceQualifier ) ){
                                    deviceQualifier = qualifier;
                                    savePairedDevice();
                                }
                            }

                            m = regexDeviceTemperature.matcher(response);
                            if( m.find() ){
                                deviceTemperature = m.group(1);
                            }

                            m = regexRemainingMilliVolts.matcher(response);
                            if( m.find() ){
                                deviceVoltage = m.group(1);
                            }
                        }
                    }
                    catch(IOException e){
                        close();
                    }
                    // Log.d( "Hippo", HippoDevice.this.toString() );
                }
            }).start();
        }
    }


    private void loadStoredDevices(){
        SharedPreferences prefs = ctx.getSharedPreferences(ctx.getResources().getString(R.string.app_prefs), Context.MODE_PRIVATE);

        for (String deviceName : Arrays.asList("HIPPO_VF", "HIPPO_HF", "HIPPO_VB", "HIPPO_HB") ) {
            if( prefs.contains(deviceName)) pairedDevices.put( deviceName, prefs.getString( deviceName, ""));
        }

        if( pairedDevices.containsValue(device.getName())){
            for (Map.Entry<String, String> e:pairedDevices.entrySet() ){
                if( e.getValue().contentEquals( device.getName() )) {
                    switch ( e.getKey() ){
                        case "HIPPO_VF": deviceQualifier = "VF"; break;
                        case "HIPPO_HF": deviceQualifier = "HF"; break;
                        case "HIPPO_VB": deviceQualifier = "VB"; break;
                        case "HIPPO_HB": deviceQualifier = "HB"; break;
                    }
                }
            }

        } else {
            // Connect and find out qualifier...
            new Thread(new Runnable() {
                @Override
                public void run() {
                    device.createBond();
                    createSecureSocket();
                }
            });

            //flushBuffer();
            //updateInfo();
        }
    }

    private void savePairedDevice(){
        SharedPreferences prefs = ctx.getSharedPreferences(ctx.getResources().getString(R.string.app_prefs), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= prefs.edit();

        switch( deviceQualifier.toLowerCase() ){
            case "vf": editor.putString( "HIPPO_VF", device.getName() ); break;
            case "hf": editor.putString( "HIPPO_HF", device.getName() ); break;
            case "vb": editor.putString( "HIPPO_VB", device.getName() ); break;
            case "hb": editor.putString( "HIPPO_HB", device.getName() ); break;
        }

        editor.apply();
    }


    @Override
    public String toString() {

        String r = "Hippo device:\n";
        r += "--------------------------------------\n";
        r += "name\t\t\t\t: " + device.getName() + "\n";
        r += "MAC address\t\t\t: " + device.getAddress() + "\n";
        r += "Socket is\t\t\t: " + (socket.isConnected() ? "open." : "closed." ) + "\n";
        r += "Date/time\t\t\t: " + deviceDateTime + "\n";
        r += "Qualifier\t\t\t: " + deviceQualifier + "\n";
        r += "Temperature\t\t\t: " + deviceTemperature + "\n";
        r += "Remaining voltage\t: " + deviceVoltage + "mV.\n";

        return r;
    }
}
