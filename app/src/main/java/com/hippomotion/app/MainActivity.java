package com.hippomotion.app;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;


public class MainActivity extends AppCompatActivity implements Hippo.HippoListener, View.OnClickListener {

    private static final Boolean NOLOGGING = false;
    private static final int     MAX_ENTRIES = 64;
    private static final int     INTENT_ENABLE_BT = 1024;

    List<String> logEntries;
    ListView logView;
    ArrayAdapter<String> listAdapter;
    List<TextView> hippoTextViews;
    Button start, pause;

    BluetoothAdapter blueAdapter;

    Hippo hippo;

    /* TODO's
     * Have a statusListener instead of updating when actions are made...
     * ACL_CONNECTED Does not mean it has re-paired it, it just means WE re-connected to it.
     * Using insecure did not do it, find another solution to entering pin without userinteraction.
     *
     * Ignore detecting in vicinity, just run the commands and it isn't extremely important if ALL is stopped...
     *  stop remaining anyway.
     *
     * Start however MUST, and as synchronized as possible.
     *
    * */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Let's log to screen...
        logEntries = new ArrayList<>();
        listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, logEntries);
        logView = (ListView) findViewById(R.id.listView);
        logView.setAdapter(listAdapter);
        hippoTextViews = new ArrayList<>();

        for (int id : Arrays.asList( R.id.hippo_fl, R.id.hippo_fr, R.id.hippo_bl, R.id.hippo_br)) {
            hippoTextViews.add((TextView) findViewById(id));
        }

        setState(null, "Searching");

        start = (Button) findViewById(R.id.hippo_start);
        pause = (Button) findViewById(R.id.hippo_pause);
        start.setOnClickListener( this );
        pause.setOnClickListener(this);

        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE));
        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_REQUEST_ENABLE));
        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_LOCAL_NAME_CHANGED));

        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED));
        //registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
        //registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_UUID));
        //registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_NAME_CHANGED));
        //registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_CLASS_CHANGED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_PAIRING_REQUEST) );

        hippo = new Hippo(this);

        blueAdapter = BluetoothAdapter.getDefaultAdapter();

        if ( blueAdapter == null ) {
            log("No bluetooth support.");
        }
        else if ( !blueAdapter.isEnabled() ) {
            log("Bluetooth inactive.");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, INTENT_ENABLE_BT);
        }
        else {
            if( !addPaired() ){
                if( blueAdapter.isDiscovering() ){
                    log("Already scanning...");
                }
                else {
                    blueAdapter.startDiscovery();
                }
            }
            else{

                if( blueAdapter.isDiscovering() ) {
                    blueAdapter.cancelDiscovery();
                }

                hippo.connectAll();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case INTENT_ENABLE_BT:

                if ( resultCode == Activity.RESULT_OK ) {

                    if( !addPaired() ){
                        log("Bluetooth activated, scanning...");
                        blueAdapter.startDiscovery();
                    }
                    else{
                        log("All devices paired.");
                        hippo.connectAll();
                    }

                } else if ( resultCode == Activity.RESULT_CANCELED ) {
                    log("Bluetooth permission declined.");
                }

                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId() ){
            case R.id.hippo_refresh:

                if( !addPaired() ) {
                    if( !blueAdapter.isDiscovering() ){
                        blueAdapter.startDiscovery();
                    }

                } else if( !hippo.isAllDevicesConnected() ){
                    if( blueAdapter.isDiscovering() ){
                        blueAdapter.cancelDiscovery();
                    }

                    hippo.connectAll();

                } else {

                    logEntries.clear();
                    listAdapter.notifyDataSetChanged();
                }

                break;

            default: return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    protected void onDestroy() {

        blueAdapter.cancelDiscovery();
        unregisterReceiver(mReceiver);
        hippo.disConnectAll();

        super.onDestroy();
    }

    // ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * **


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.hippo_start:
                if( start.getText().toString().toLowerCase().contentEquals("start") ){
                    // SETTIME,<year>,<month>,<day>,<hour>,<min>,<sec>
                    Date timeStamp = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat( "yy,MM,dd,hh,mm,ss", Locale.getDefault() );
                    String setTimeCmd = String.format("SETTIME,%s", ft.format(timeStamp));

                    // When we've received response from ALL SETTIME's, onReadyForStart will be triggered.
                    hippo.sendCommand( setTimeCmd );

                } else {
                    hippo.sendCommand("STOP");
                    start.setText( getResources().getString(R.string.START) );
                }

                break;

            case R.id.hippo_pause:
                if( pause.getText().toString().contentEquals("Pause") ){
                    hippo.sendCommand("Pause");
                    start.setText( getResources().getString(R.string.RESUME) );
                } else {
                    hippo.sendCommand("RESUME");
                    start.setText( getResources().getString(R.string.PAUSE));
                }

                break;

            default: log( "Unknown/unhandled click!" );
        }
    }

    // ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * **

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            //Parcelable[] uuids = intent.getParcelableArrayExtra(BluetoothDevice.EXTRA_UUID);
            //BluetootDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            BluetoothDevice device;

            switch ( intent.getAction() ){

                case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                    // resume.setBackgroundColor( 0x55F59224);
                    break;

                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:

                    if( !hippo.isAllDevicesDetected() ){
                        //resume.setBackgroundColor( 0xBBF59924);
                        blueAdapter.startDiscovery();
                        break;
                    }
                    //else{
                        //resume.setBackgroundColor(0x5500AA00);
                    //}

                    break;

                case BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED: Log.d("Hippo", "State changed."); break;
                case BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE: Log.d("Hippo", "Requested discoverable"); break;
                case BluetoothAdapter.ACTION_REQUEST_ENABLE: Log.d("Hippo", "Requested enable"); break;
                case BluetoothAdapter.ACTION_SCAN_MODE_CHANGED: Log.d("Hippo", "Scan mode changed"); break;
                case BluetoothAdapter.ACTION_STATE_CHANGED: Log.d("Hippo", "State changed"); break;
                case BluetoothAdapter.ACTION_LOCAL_NAME_CHANGED: Log.d("Hippo", "Name changed."); break;

                //          ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **

                case BluetoothDevice.ACTION_ACL_CONNECTED:
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    setState( hippo.getQualifierByName( device.getName() ), "Connected" );
                    Log.d("Hippo", "Device " + hippo.getQualifierByName(device.getName()) + " connected.");

                    break;

                case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    // Whaaaat! Whyyyyyyy!
                    Log.d( "Hippo", "device: "+ hippo.getQualifierByName(device.getName())+" just disconnected itsealf." );
                    hippo.disConnectDeviceByName(device.getName());
                    setState(hippo.getQualifierByName(device.getName()), "Disconnected");
                    onNotAllConnected();

                    break;

                case BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED:
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    Log.d("Hippo", "Disconnect requested: " + device.getName());

                    break;

                case BluetoothDevice.ACTION_FOUND:
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    String devName = device.getName();

                    // We're looking for four specific devices in particular...HIPPOS
                    if( devName!=null && devName.length()>0 && devName.matches("^HIP.*$") ) {

                        Log.d("Hippo", "Detected device " + device.getName() + ", mac: " + device.getAddress());
                        hippo.addDevice(device);

                        if(hippo.isAllDevicesDetected()){

                            Log.d("Hippo", "All devices detected..." );

                            if( blueAdapter.isDiscovering() ){
                                Log.d("Hippo", "Stop scanning...");
                                blueAdapter.cancelDiscovery();
                            }

                            hippo.connectAll();
                        }
                    }

                    break;

                case BluetoothDevice.ACTION_PAIRING_REQUEST:
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    Log.d( "Hippo", "Pairing requested for device " + device.getName() );

                    try {
                        //int pin=intent.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY", 0);
                        //the pin in case you need to accept for an specific pin
                        Log.d( "Hippo", " " + intent.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY",0));
                        //maybe you look for a name or address
                        Log.d( "Hippo", device.getName());
                        byte[] pinBytes;
                        pinBytes = ("1234").getBytes("UTF-8");
                        device.setPin(pinBytes);
                        //setPairing confirmation if neeeded
                        // todo: What does it mean when we set pairing confirmation to true ?
                        device.setPairingConfirmation(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                default: Log.d("Hippo", "Unhandled action: " + intent.getAction());
            }
        }
    };

    // ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * **

    private boolean addPaired() {
        Set<BluetoothDevice> pairedDevices = blueAdapter.getBondedDevices();
        // If there are paired devices
        if ( pairedDevices.size() > 0 ) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {

                if ( device.getName().matches("HIP.*") ) {
                    hippo.addDevice(device);
                    Log.d("Hippo", "Found paired device " + device.getName() + " with mac " + device.getAddress());
                }
            }
        }

        return hippo.isAllDevicesDetected();
    }


    public void setState( final String designation, final String status){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if ( designation == null || designation.isEmpty() ) {
                    for (TextView t : hippoTextViews) {
                        t.setBackgroundColor(0xFFE2E2E2);
                        t.setText(status);
                        t.setTextColor(0xFFE2E2E2);
                    }
                } else {
                    switch (designation) {
                        case "VF":
                            hippoTextViews.get(0).setText(status);
                            hippoTextViews.get(0).setTextColor(colorFromState(status));
                            hippoTextViews.get(0).setBackgroundColor(colorFromState(status));
                            break;

                        case "HF":
                            hippoTextViews.get(1).setText(status);
                            hippoTextViews.get(1).setTextColor(colorFromState(status));
                            hippoTextViews.get(1).setBackgroundColor(colorFromState(status));
                            break;

                        case "VB":
                            hippoTextViews.get(2).setText(status);
                            hippoTextViews.get(2).setTextColor(colorFromState(status));
                            hippoTextViews.get(2).setBackgroundColor(colorFromState(status));
                            break;

                        case "HB":
                            hippoTextViews.get(3).setText(status);
                            hippoTextViews.get(3).setTextColor(colorFromState(status));
                            hippoTextViews.get(3).setBackgroundColor(colorFromState(status));
                            break;
                    }
                }
            }
        });
    }

    /*private void reset( final String device ){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hippo.isConnected(device);
            }
        }, 5000);
    }*/

    private int colorFromState(String state){

        switch (state.toLowerCase()){

            case "detected"    : return 0xFFE2E2E2;
            case "pairing"     : return 0xFFE2E2E2;
            case "connecting"  : return 0xFFE2E2E2;
            case "connected"   : return 0xFFFFDD15;
            //case "command sent": return 0xFF6BDA57;
            case "got response": return 0xFF6BDA57;
            case "disconnected": return 0xFFF24128;
            case "start"       : return 0xFF6BDA57;
            case "stop"        : return 0xFFFFDD15;

            default: return 0xFFE2E2E2;
        }
    }

    // When SETTIME is done... It'll trigger this method...
    public void onReadyForStart() {

        hippo.sendCommand("START");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                start.setText(getResources().getString(R.string.STOP));
            }
        });
    }

    // Observe... This can be executed within a thread
    public void onAllConnected(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                start.setEnabled(true);
            }
        });
    }

    public void onNotAllConnected(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if( start.getText().toString().contentEquals(getResources().getString(R.string.START))){
                    start.setEnabled(false);
                }
            }
        });
    }

    // ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * **

    public void uiLog( final String format, final Object ... args ){
        if( NOLOGGING ) return;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                log( format, args);
            }
        });
    }

    public void log( String format, Object ... args ){
        if( NOLOGGING ) return;

        String methodName = new Exception().getStackTrace()[1].getMethodName();

        if( methodName.contains("$")) methodName = new Exception().getStackTrace()[2].getMethodName();

        String preEntry = String.format( "%s() - ", methodName );
        try {
            logEntries.add( 0, preEntry + String.format(format, args) );
        } catch (Exception e){
            logEntries.add(0, preEntry + "String.format error: " + e.getMessage());
        }

        if( logEntries.size()>MAX_ENTRIES ) logEntries.remove(0);

        listAdapter.notifyDataSetChanged();
        //logView.smoothScrollToPosition( listAdapter.getCount() );
    }

    // ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * ** * **
}
