package com.hippomotion.app;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;

/**
 * Created by Jyrki on 2016-02-08.
 *
 */

// todo: Have a getDeviceByMac, deviceByDesignation and byName.
public class Hippo {
    // Keep count of settime events (if four events is triggered, then onReadyForStart() )
    static int SET_TIME_EVENT = 1;

    private HippoListener mListener;

    List<HippoDevice> devices;

    public interface HippoListener{
        void log( String format, Object ... args );
        void uiLog(final String format, final Object ... args);
        void setState( String designation, String status);
        void onAllConnected();
        void onNotAllConnected();
        void onReadyForStart();
    }

    public Hippo( HippoListener l){
        devices = new ArrayList<>();
        mListener = l;
    }

    public void addDevice( BluetoothDevice dev ){

        // Check if already added...
        for (HippoDevice d:devices) {
            if( d.getName().matches(dev.getName())) return;
        }

        Log.d( "Hippo", "Adding device " + dev.getName() );
        HippoDevice detectedDevice = new HippoDevice(dev, ((Context)mListener));

        devices.add( detectedDevice );

        mListener.setState(detectedDevice.getDeviceQualifier(), "Detected");
    }

    /*public HippoDevice getVF(){

        for (HippoDevice d : devices) {
            if( d.getDeviceQualifier().toLowerCase().contentEquals("vf") ){
                return d;
            }
        }

        return null;
    }

    public HippoDevice getHF(){
        for (HippoDevice d : devices) {
            if( d.getDeviceQualifier().toLowerCase().contentEquals("hf") ){
                return d;
            }
        }

        return null;
    }

    public HippoDevice getVB(){
        for (HippoDevice d : devices) {
            if( d.getDeviceQualifier().toLowerCase().contentEquals("vb") ){
                return d;
            }
        }

        return null;
    }

    public HippoDevice getHB(){
        for (HippoDevice d : devices) {
            if( d.getDeviceQualifier().toLowerCase().contentEquals("hb") ){
                return d;
            }
        }

        return null;
    } */

    public String getQualifierByName( String deviceName ){
        for (HippoDevice d : devices) {
            if( deviceName != null && d.getName().contentEquals(deviceName) ){
                return d.getDeviceQualifier();
            }
        }

        return "";
    }

    /*public boolean isConnected( String designation){
        for (final HippoDevice d : devices) {
            if ( d.getDeviceQualifier().matches(designation) ) {
                if( d.isConnected() ){
                    mListener.setState(d.getDeviceQualifier(), "Connected" );
                    return true;
                }
                else{
                    mListener.setState(d.getDeviceQualifier(), "Disconnected" );
                    return false;
                }
            }
        }

        return false;
    } */

    public void connectAll(){

        if( devices == null || devices.size() < 4 ) {
            //mListener.log( "Device list is null or insufficient." );
            return;
        }

        Log.d( "Hippo", "(Re)connecting all devices..." );

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (HippoDevice d:devices) {

                    if( d.isConnected() ){
                        mListener.setState(d.getDeviceQualifier(), "Connected" );
                        Log.d("Hippo", "Device " + d.getDeviceQualifier() + " connected." );
                        continue;
                    }

                    mListener.setState(d.getDeviceQualifier(), "Connecting" );
                    Log.d("Hippo", "Connecting device " + d.getDeviceQualifier() + ".");

                    String result = d.createSecureSocket().toLowerCase();
                    if ( "connected".equals(result.toLowerCase()) ){
                        mListener.setState(d.getDeviceQualifier(), "Connected" );
                        Log.d("Hippo", "Device " + d.getDeviceQualifier() + " connected.");
                    }
                    else{
                        mListener.setState(d.getDeviceQualifier(), "Disconnected");
                        Log.d("Hippo", "Device " + d.getDeviceQualifier() + " DIS-connected.");
                        //mListener.uiLog("%s->%s", d.getDesignation(), result);
                    }
                }

                refreshConnectionStates();

            }
        }).start();
    }

    public void refreshConnectionStates(){
        boolean allConnected = true;
        boolean someConnected = false;

        for (HippoDevice d:devices) {
            //mListener.uiLog( d.toString() );
            if( !d.isConnected() ){
                mListener.setState(d.getDeviceQualifier(), "Disconnected" );
                allConnected = false;
            }
            else{
                mListener.setState(d.getDeviceQualifier(), "Connected" );
                someConnected = true;
            }
        }

        if( allConnected ){
            // allConnected doesn't mean all devices got updated info.
            mListener.onAllConnected();
        } else if( !someConnected ){
            //mListener.onNoneConnected();
        }
    }

    public void disConnectDeviceByName( String deviceName ){

        if(devices == null || devices.isEmpty() ) return;

        for (HippoDevice d:devices) {
            if( d.getName().contentEquals(deviceName)){
                d.close();
                //mListener.setState(d.getDeviceQualifier(), "Disconnected");
            }
        }
    }

    public void disConnectAll(){

        if(devices == null || devices.isEmpty() ) return;

        for (HippoDevice d:devices) {
            if( d.isConnected() ) d.close();
            //mListener.onNoneConnected();
        }
    }

    public boolean isAllDevicesDetected() {
        return devices.size()==4;
    }

    public boolean isAllDevicesConnected(){
        for (HippoDevice d: devices) if( !d.isConnected() ) return false;
        mListener.onAllConnected();
        return true;
    }

    public void sendCommand( final String cmd ){

        new Thread(new Runnable() {
            @Override
            public void run() {

                boolean allConnected = true;
                // Check if all is connected, if not try to reconnect...

                for (final HippoDevice d: devices) {
                    if (!d.isConnected()){
                        mListener.setState( d.getDeviceQualifier(), "Connecting" );
                        mListener.onNotAllConnected();
                    }
                }

                for (final HippoDevice d: devices) {

                    if ( !d.isConnected() ){

                        String result = d.createSecureSocket().toLowerCase();

                        if ( "connected".equals(result.toLowerCase()) ){
                            mListener.setState(d.getDeviceQualifier(), "Connected");
                            //Log.d("Hippo", "Device " + d.getDeviceQualifier() + " connected.");
                        } else {
                            mListener.setState(d.getDeviceQualifier(), "Disconnected");
                            Log.d("Hippo", "Device " + d.getDeviceQualifier() + " DIS-connected.");
                            //mListener.uiLog( result );
                            allConnected = false;
                        }
                    }
                }

                if( !allConnected && (cmd.toLowerCase().equals("start") /*|| cmd.toLowerCase().equals("stop")*/ ) ) return;
                if (allConnected){
                    mListener.onAllConnected();
                }

                List<Thread> runnables = new ArrayList<>();
                CountDownLatch latch = new CountDownLatch(1);

                boolean someConnected = false;

                for (final HippoDevice d: devices) {
                    if( !d.isConnected() ){
                        //mListener.uiLog( "Oups! Device %s not connected.", d.getDeviceQualifier() );
                        // BroadcastReceiver is slow, let's set state noow.
                        //mListener.setState(d.getDeviceQualifier(), "Disconnected" );
                        continue;
                    }
                    else someConnected = true;

                    if( !someConnected ) {
                        mListener.onNotAllConnected();
                    }

                    runnables.add( new Thread( new HippoThread(latch,d,cmd)));
                }

                for ( Thread r : runnables ) {
                    r.start();
                }

                latch.countDown();

            }
        }).start();
    }


    class HippoThread implements Runnable{

        private int TIMEOUT_IN_MILLI = 2000;

        final Pattern regex = Pattern.compile(".*(ERR=\\d)$", Pattern.DOTALL | Pattern.MULTILINE);
        byte[] buffer = new byte[2048];

        CountDownLatch latch;
        HippoDevice dev;

        InputStream is;
        OutputStream os;

        String cmd;

        int bytes = 0;
        String response = "";

        HippoThread( CountDownLatch latch, HippoDevice d, String command){
            this.latch = latch;
            dev = d;
            cmd = command;
            is = dev.getInputStream();
            os = dev.getOutputStream();
        }

        @Override
        public void run() {

            try {
                // Sometimes there is leftovers in the buffer, read it off.
                dev.flushBuffer();
                //mListener.uiLog("Sending cmd:%s to %s", cmd, dev.getDeviceQualifier());

                try{
                    latch.await();
                }catch(InterruptedException e){
                    //
                }

                //Log.d( "Hippo", "" + System.currentTimeMillis() );
                //mListener.uiLog( "%d", System.currentTimeMillis() );

                os.write((cmd + "\n").getBytes());
                os.flush(); // wait for it...

                mListener.setState(dev.getDeviceQualifier(), cmd);
                Log.d("Hippo", "Sent command " + cmd + " to device " + dev.getDeviceQualifier() + ".");

                long timeout = System.currentTimeMillis() + TIMEOUT_IN_MILLI;

                do {
                    if ( is.available() > 0 ) {
                        bytes = is.read(buffer);
                        //response += new String(buffer, 0, bytes, "UTF-8");
                        for( int i = 0; i<bytes;i++){
                            if( buffer[i] > 13) response += String.format( "%c", buffer[i] );
                            if( buffer[i] == 10 && !(buffer[i+1]==10 || buffer[i+1]==13) ) response += System.getProperty("line.separator"); //System.lineSeparator();
                        }

                        for (int i = 0; i < buffer.length; i++) {
                            buffer[i] = '\0';
                        }
                    }

                    if ( System.currentTimeMillis() > timeout ) {
                        break;
                    }

                } while (!regex.matcher(response).find());

                // När ERR=? träffats och siffran inte är 0, då såg jag felmeddelandet (batteriet nära slutet) direkt efter...
                //  När vet jag slut på meddelandet? "\n\r" eller?
                //  De kollar, jag tillfäligt släppa.

                // En av enheterna Timeout:ar nästan konsekvent... BÖR jag definitivt skicka \n\r i alla lägen?
                //  Svar: SKALL räcka med \n ( när jag skickar och slutar skickar timeout:ar enheten efter en millisec... MÅSTE inte skicka \n ens.)

                // ALLTID ta emot ERR=?\n\r Och absolut inget mer?
                // Eller ignorerar jag allt efter ERR=? ( som riskerar att buffras inför nästa respons)

                // GetData... Hur hämta och ta bort filer?  Nästa version.
                // KRÄVER kortet/sensorn att man skickar PIN, eller är det protokollet? Svar: Japp, kortet/sensorn kräver.

                if ( System.currentTimeMillis() > timeout ) {
                    mListener.uiLog("%s->%s response timeout.", dev.getDeviceQualifier(), cmd);
                    Log.d("Hippo", "Response timeout for device " + dev.getDeviceQualifier() + " with command " + cmd);
                } else {
                    //mListener.setState(dev.getDeviceQualifier(), "Got response");
                    Log.d("Hippo", "Got response from device " + dev.getDeviceQualifier());
                }

                //mListener.uiLog("Received %d bytes from %s:\n%s", bytes, dev.getDeviceQualifier(), response);
                Log.d("Hippo", "Received " + response.length() + " bytes from " + dev.getDeviceQualifier() + ". Message :" + response);

                // If command begins with SETTIME, and this is the fourth to be executed trigger onReadyForStart()
                if( cmd.contains("SETTIME") && ++SET_TIME_EVENT>4 ){
                    SET_TIME_EVENT = 1;
                    mListener.onReadyForStart();
                }

            } catch (IOException e) {
                mListener.uiLog("IOException sending/receiving from %s:\n%s", dev.getDeviceQualifier(), e.getMessage());
                dev.close();
                mListener.setState(dev.getDeviceQualifier(), "Disconnected");
            }
        }
    }
}
